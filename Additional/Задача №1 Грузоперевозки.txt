Задача №1
«Грузоперевозки»

Ограничение по времени 2 сек.
Ограничение по памяти 256 Мб

Судоходная компания предлагает два вида транспорта для перевозки сыпучих грузов. 
Грузовик первого типа может перевезти Q1 тонн груза за одну поездку. 
Разовая поездка стоит P1, и цена не зависит от уровня загрузки транспортного средства. 
Для грузовика второго типа эти значения равны Q2 и P2 соответственно.

Найдите минимальную стоимость перевозки A тонн груза.

Формат ввода
Входные данные содержат пять натуральных чисел, не превышающих тысячи: Q1, P1, Q2, P2, A. Числа разделены пробелами.

Формат вывода
Выведите одно число: минимально возможную цену.

Пример
Вход
3 20 20 100 21

Выход
120
